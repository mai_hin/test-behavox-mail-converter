Project:
There are three customers with emails data type. They provide them to us in different formats:

Customer A simply stores emails in MBOX format (http://qmail.org./man/man5/mbox.html).
Customer B store emails in MBOX format too but then zip file in addition.
Customer C do not zip files but archive emails from the special archiving mailbox. Original emails in this mailbox are wrapped as mail attachments to keep original sender and receiver.

Arguments (need to be provided in properties):
    in - mandatory, path to input directory with mail files
    out - optional, path to output directory for normalized files (current dir by default)
    format - optional, format of messages in inputDir. Available values: Mbox, WMbox (Mbox by default)
	zipped - optional, true if messages from customer are zipped. Available values: true, false(false by default)

List of available headers in output message can be configured in config.properties file in property "headers-list"

example: -Din="C:\Users\tkacsve\mail-converter\test-behavox-mail-converter\src\main\resources\DirB" -Dout="H:\test-behavox-mail-converter\out" -Dformat="Mbox" -Dzipped="true"