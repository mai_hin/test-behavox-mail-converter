package test.task.converter.parsers;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import javafx.util.Pair;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.metadata.TikaCoreProperties;
import org.apache.tika.parser.mbox.MboxParser;
import org.apache.tika.sax.BodyContentHandler;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Stream;

import static org.apache.tika.parser.mbox.MboxParser.EMAIL_FROMLINE_METADATA;
import static org.apache.tika.parser.mbox.MboxParser.EMAIL_HEADER_METADATA_PREFIX;

public abstract class AbstractMailParser {
    public static String LINE_SEPARATOR = System.getProperty("line.separator");

    protected final Logger logger = LogManager.getLogger(getClass());
    private SimpleDateFormat formatter = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss Z", Locale.US);

    protected static Map<String, String> tikaMetadataToHeaderPropertyMap = ImmutableMap.<String, String>builder()
            .put(TikaCoreProperties.CREATOR.getName(), "From")
            .put(Metadata.MESSAGE_RECIPIENT_ADDRESS, "To")
            .put(Metadata.MESSAGE_CC, "Cc")
            .put(Metadata.MESSAGE_BCC, "Bcc")
            .put(Metadata.SUBJECT, "Subject")
            .put(TikaCoreProperties.CREATED.getName(), "Date")
            .put(TikaCoreProperties.IDENTIFIER.getName(), "Message-Id")
            .put(TikaCoreProperties.RELATION.getName(), "In-Reply-To")
            .put(TikaCoreProperties.FORMAT.getName(), "Content-Type")
            .build();

    abstract Pair<Map<String, String>, BodyContentHandler> parseMessage(StringBuilder mail);

    // Method parses inputStream: split messages by MBOX_RECORD_DIVIDER, parse it one by one by provided tika-parser
    // Return extracted headers in metadata object and body in bodyContentHandler object
    public List<Pair<Map<String, String>, BodyContentHandler>> parseInputStream(InputStream input) {
        List<Pair<Map<String, String>, BodyContentHandler>> parsedMessagesList = new ArrayList<>();
        StringBuilder mail = new StringBuilder();
        try (BufferedReader buffer = new BufferedReader(new InputStreamReader(input))) {
            buffer.lines().forEach(line -> {
                if (line.startsWith(MboxParser.MBOX_RECORD_DIVIDER) && mail.length() != 0) {
                    parsedMessagesList.add(parseMessage(mail));
                    mail.delete(0, mail.length());
                }
                mail.append(line).append(LINE_SEPARATOR);
            });
            parsedMessagesList.add(parseMessage(mail));
        } catch (IOException e) {
            logger.warn("Exception is thrown during parsing", e);
        }
        return parsedMessagesList;
    }

    protected Map<String, String> parseMetaData(Collection<Metadata> fullMetadata) {
        Map<String, String> metadata = new HashMap<>();
        for (Metadata m : fullMetadata) {
            Stream.of(m.names())
                    .filter(n -> tikaMetadataToHeaderPropertyMap.containsKey(n) ||
                            n.startsWith(EMAIL_HEADER_METADATA_PREFIX) && !EMAIL_FROMLINE_METADATA.equals(n))
                    .forEach(n -> {
                        if (n.startsWith(EMAIL_HEADER_METADATA_PREFIX)) {
                            metadata.putIfAbsent(n.substring(EMAIL_HEADER_METADATA_PREFIX.length()), m.get(n));
                        } else {
                            metadata.putIfAbsent(tikaMetadataToHeaderPropertyMap.get(n),
                                    TikaCoreProperties.CREATED.getName().equals(n) ? convertDate(m.getDate(TikaCoreProperties.CREATED)) : m.get(n));
                        }
                    });
        }
        return metadata;
    }

    protected String convertDate(Date date) {
        return formatter.format(date);
    }
}
