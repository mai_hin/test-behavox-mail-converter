package test.task.converter.parsers;

import javafx.util.Pair;
import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.AutoDetectParser;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.RecursiveParserWrapper;
import org.apache.tika.parser.html.HtmlParser;
import org.apache.tika.sax.BasicContentHandlerFactory;
import org.apache.tika.sax.BodyContentHandler;
import org.xml.sax.SAXException;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class WrappedMboxParser extends AbstractMailParser {
    private RecursiveParserWrapper parser = new RecursiveParserWrapper(new AutoDetectParser(),
            new BasicContentHandlerFactory(BasicContentHandlerFactory.HANDLER_TYPE.BODY, -1));

    @Override
    protected Pair<Map<String, String>, BodyContentHandler> parseMessage(StringBuilder mail) {
        InputStream inputStream = new ByteArrayInputStream(mail.toString().getBytes());
        try {
            parser.reset();
            parser.parse(inputStream, null, new Metadata(), new ParseContext());
        } catch (IOException | TikaException | SAXException e) {
            logger.warn("Can't parse message due to exception, skipping {}", mail, e);
            return null;
        }
        List<BodyContentHandler> bodies = new ArrayList<>();
        for (Metadata metadata : parser.getMetadata()) {
            for (String value : metadata.getValues("X-TIKA:content")) {
                try {
                    BodyContentHandler textHandler = new BodyContentHandler();
                    InputStream is = new ByteArrayInputStream(value.getBytes("UTF-8"));
                    new HtmlParser().parse(is, textHandler, new Metadata(), new ParseContext());
                    bodies.add(textHandler);
                } catch (Exception e) {
                    logger.warn("Can't extract message body {}", mail, e);
                }
            }
        }
        Map<String, String> metadata = parseMetaData(parser.getMetadata());
        if (bodies.isEmpty()) {
            return new Pair<>(metadata, null);
        }
        if (bodies.size() > 1) {
            logger.info("Found more then one bodies in message. Will return first");
        }
        return new Pair<>(metadata, bodies.get(0));
    }

}
