package test.task.converter.parsers;

import javafx.util.Pair;
import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.sax.BodyContentHandler;
import org.xml.sax.SAXException;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

public class CustomMboxParser extends AbstractMailParser {
    private org.apache.tika.parser.mbox.MboxParser parser = new org.apache.tika.parser.mbox.MboxParser();

    public CustomMboxParser() {
        parser.setTracking(true);
    }

    @Override
    protected Pair<Map<String, String>, BodyContentHandler> parseMessage(StringBuilder mail) {
        InputStream inputStream = new ByteArrayInputStream(mail.toString().getBytes());
        BodyContentHandler handler = new BodyContentHandler();
        try {
            parser.parse(inputStream, handler, new Metadata(), new ParseContext());
        } catch (IOException | TikaException | SAXException e) {
            logger.warn("Can't parse message due to exception, skipping", e);
            return null;
        }
        Map<String, String> metadata = parseMetaData(parser.getTrackingMetadata().values());
        return new Pair<>(metadata, handler);

    }
}
