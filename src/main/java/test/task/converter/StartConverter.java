package test.task.converter;

import com.google.common.base.Objects;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class StartConverter {
    private static final Logger logger = LogManager.getLogger(StartConverter.class);

    public static void main(String args[]) throws Exception {
        logger.debug("=============  Test task for Behavox  =================");

        String inputDir = System.getProperty("in");
        if (StringUtils.isBlank(inputDir)) {
            logger.warn("'in' property is empty. Please check and try one more time");
            return;
        }

        String outputDir = Objects.firstNonNull(System.getProperty("out"), "");

        MessageConverter.Format format;
        String f = System.getProperty("format");
        if (f == null) {
            logger.info("'format' parameter is empty, will convert by standard MBox parser");
            format = MessageConverter.Format.Mbox;
        } else {
            try {
                format = MessageConverter.Format.valueOf(f);
            } catch (IllegalArgumentException e) {
                logger.warn("Unknown format {}. Avalable values: \n\t Mbox - standard .mbox files \n\tWMbox - .mbox files with wrapped emails", f);
                return;
            }
        }

        boolean zipped = false;
        String z = System.getProperty("zipped");
        if (z != null) {
            try {
                zipped = Boolean.valueOf(z);
            } catch (IllegalArgumentException e) {
                logger.warn("Unknown value inside zipped property: {}. Will convert with default value (unzipped)", z);
                return;
            }
        }
        MessageConverter converter = new MessageConverter();
        converter.convertMessages(inputDir, outputDir + "\\", format, zipped);
    }
}

