package test.task.converter;

import com.google.common.collect.ImmutableList;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.openxml4j.opc.internal.FileHelper;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;

public class RFC822FormatHelper {
    private static final Logger logger = LogManager.getLogger(RFC822FormatHelper.class);

    static private List<String> availableHeaders;

    static {
        Properties prop = new Properties();
        try {
            InputStream inputStream = FileHelper.class.getClassLoader().getResourceAsStream("config.properties");
            prop.load(inputStream);
            availableHeaders = Arrays.asList(((String) prop.get("headers-list")).split(","));
            logger.info("Loaded {} values to availableHeaders list", availableHeaders.size());

        } catch (Exception e) {
            logger.info("Can't load properties file. Will use default values for availableHeaders list");
            availableHeaders = Arrays.asList("Return-Path", "Received", "Resent-Date", "Resent-From", "Resent-Sender", "Resent-To", "Resent-Cc", "Resent-Bcc",
                            "Resent-Message-Id", "Date", "From", "Sender", "Reply-To", "To", "Cc", "Bcc", "Message-Id", "In-Reply-To", "References",
                            "Subject", "Comments", "Keywords", "Errors-To", "MIME-Version", "Organization", "Content-Type", "Content-Transfer-Encoding", "Content-MD5", ":",
                            "Content-Length", "Status", "User-Agent"
                    );
        }
    }

    //    Header fields  are  NOT  required  to
    //    occur  in  any  particular  order, except that the message
    //    body must occur AFTER  the  headers.   It  is  recommended
    //    that,  if  present,  headers be sent in the order "Return-
    //    Path", "Received", "Date",  "From",  "Subject",  "Sender",
    //            "To", "cc", etc.
    private static ImmutableList<String> headersShouldBeFirstIfPresent = ImmutableList.<String>builder()
            .add("Return-Path")
            .add("Received")
            .add("Date")
            .add("From")
            .add("Subject")
            .add("Sender")
            .add("To")
            .add("Cc")
            .build();

    public static List<String> getHeadersShouldBeFirstIfPresent() {
        return headersShouldBeFirstIfPresent;
    }

    public static boolean isAvailable(String header) {
        return availableHeaders.contains(header);
    }
}
