package test.task.converter;

import javafx.util.Pair;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.tika.sax.BodyContentHandler;
import test.task.converter.parsers.AbstractMailParser;
import test.task.converter.parsers.CustomMboxParser;
import test.task.converter.parsers.WrappedMboxParser;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.zip.ZipFile;

public class MessageConverter {
    private static int threadCount = 20;
    private static final Logger logger = LogManager.getLogger(MessageConverter.class);


    public void convertMessages(String inputDir, String outputDir, Format format, boolean zipped) {
        logger.debug("Starting convert messages by {} converter, zipped = {}", format, zipped);
        List<File> messages = getFilesInFolder(inputDir);
        if (messages == null) {
            logger.info("Input folder {} is not found", inputDir);
            return;

        }
        logger.debug("Found {} files in input folder {}", messages.size(), inputDir);

        int count = 0;
        while (count < messages.size()) {
            final int finalCount = count;
            IntStream.range(0, Math.min(threadCount, messages.size() - count))
                    .parallel()
                    .forEach(i -> {
                        AbstractMailParser parser = createParser(format);
                        if (zipped) {
                            try (ZipFile zipFile = new ZipFile(messages.get(finalCount + i))) {
                                String prefixName = outputDir + zipFile.getName().substring(zipFile.getName().lastIndexOf("\\") + 1) + ".";
                                zipFile.stream().forEach(entry -> {
                                    try (InputStream is = zipFile.getInputStream(entry)) {
                                        parseAndSave(parser, is, prefixName + entry.getName());
                                    } catch (IOException e) {
                                        logger.warn("Can't read zip entry {} due to exception. Will skip it", entry.getName(), e);
                                    }
                                });
                            } catch (IOException e) {
                                logger.warn("Can't read zip file {} due to exception. Will skip it", messages.get(finalCount + i), e);
                            }
                        } else {
                            try (FileInputStream is = new FileInputStream(messages.get(finalCount + i))) {
                                String prefixName = outputDir + messages.get(finalCount + i).getName();
                                parseAndSave(parser, is, prefixName);
                            } catch (IOException e) {
                                logger.warn("Can't read file {} due to exception. Will skip it", messages.get(finalCount + i), e);
                            }
                        }
                    });
            count = count + threadCount;
        }
    }

    private void parseAndSave(AbstractMailParser parser, InputStream is, String prefixName) {
        logger.debug("Start parsing message from {}", prefixName);
        List<Pair<Map<String, String>, BodyContentHandler>> parsedPair = parser.parseInputStream(is);
        int c = 0;
        for (Pair<Map<String, String>, BodyContentHandler> pair : parsedPair) {
            if (pair == null) {
                continue;
            }
            convert2RFC822AndSave(pair.getKey(), pair.getValue(), prefixName + (++c) + ".eml");
        }
    }


    private void convert2RFC822AndSave(Map<String, String> metadata, BodyContentHandler handler, String fileName) {
        try (Writer w = new OutputStreamWriter(new FileOutputStream(fileName, false), "UTF-8")) {
            List<String> priorityHeaders = RFC822FormatHelper.getHeadersShouldBeFirstIfPresent();
            for (String header : priorityHeaders) {
                String value = metadata.remove(header);
                if (value != null && RFC822FormatHelper.isAvailable(header)) {
                    w.write(header + ": " + value);
                    w.write(AbstractMailParser.LINE_SEPARATOR);
                }
            }
            for (Map.Entry<String, String> entry : metadata.entrySet()) {
                if (RFC822FormatHelper.isAvailable(entry.getKey())) {
                    w.write(entry.getKey() + ": " + entry.getValue());
                    w.write(AbstractMailParser.LINE_SEPARATOR);
                }
            }
            if (handler != null) {
                w.write(AbstractMailParser.LINE_SEPARATOR);
                w.write(handler.toString());
            }
            logger.debug("Message was successfully converted and saved to {}", fileName);
        } catch (IOException e) {
            logger.warn("Can't save converted message to file {} due to exception. Will skip it", fileName, e);
        }
        return;
    }

    private List<File> getFilesInFolder(String folderName) {
        try {
            return Files.walk(Paths.get(folderName))
                    .filter(Files::isRegularFile)
                    .map(Path::toFile)
                    .filter(is -> is != null)
                    .collect(Collectors.toList());
        } catch (IOException e) {
            logger.error("Can't find input directory: {}", folderName, e);
            return null;
        }
    }


    private AbstractMailParser createParser(Format format) {
        if (format == Format.WMbox) {
            return new WrappedMboxParser();
        }
        return new CustomMboxParser();
    }

    enum Format {
        Mbox, WMbox
    }
}
